# Ground Beef

### Ingredients

`4c diced cauliflower`

`1c diced walnuts`

`3/4 c tomato sauce`

`1 tbs coconut aminos`

`1 tsp paprika`

`1 tsp sea salt`

`1/8 tsp white or black pepper`

`1/4 tsp chili flakes`


### Instructions

Combine Cauliflower, walnuts and tomato sauce. Mix with spices.

Put in oven at 350 degree F for 30 mins. Stir. Check and stir every 15 mins until done. 



### Source
https://cookingwithplants.com/recipe/vegan-ground-meat-recipe/