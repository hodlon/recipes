# Ranch Dressing

### Ingredients

`1 cup unsalted raw cashews`

`1/2 cup alternative milk (rice milk or non-sweetened milkadamea works best. Nothing sweet)`

`2 tbsp lemon juice`

`1 tsp salt`

`1/2 tsp ground black pepper`

`1/2 tsp garlic powder`

`1/2 tsp onion powder`

`1/4 tsp paprika`


### Instructions

Soak cashews for 4 hours in lukewarm water, rinsing the water often.

Add all the ingredients to a blender and blend until smooth.

Store if refridgerator.


### Source

https://simpleveganblog.com/simple-vegan-ranch-dressing/